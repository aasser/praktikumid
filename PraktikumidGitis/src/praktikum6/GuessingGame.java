package praktikum6;

import praktikum1.TextIO;

public class GuessingGame {

	public static void main(String[] args) {

		int randomnumber = GuessingGame.randomnumber(1, 100);


		while (true) {

			int kasutajaSisestus = GuessingGame.kasutajaSisestus("Suvaline arv", 1, 100);
			System.out.println("Suvaline nr " + randomnumber);
			System.out.println("Kasutajasisestus nr " + kasutajaSisestus);

			if (randomnumber == kasutajaSisestus) {
				System.out.println("Panid Täppi");
				return;
			} else {
				System.out.println("Vale");
			}

		}

	}

	public static int randomnumber(int min, int max) {
		int vahemik = max - min;
		return (int) (Math.random() * vahemik) + min;

	}

	public static int kasutajaSisestus(String kysimus, int min, int max) {

		kysimus = ("Arva ära, mis number on alates " + min + " kuni " + max + ": ");
		System.out.print(kysimus);
		int sisestus = TextIO.getlnInt();
		return sisestus;

	}

}
