package praktikum2;

import praktikum1.TextIO;

public class InputNumbersAddNumbers {
	
	public static void main(String[] args) {
		
		double nr1;
		double nr2;
		double korrutis;
		
		System.out.println("Kasutaja, sisesta palun üks number");
		nr1 = TextIO.getlnDouble();
		System.out.println("Kasutaja, sisesta palun teine number");
		nr2 = TextIO.getlnDouble();
		System.out.println("Nüüd teen sellest korrutise... \nBOOM!!!");
		korrutis = nr1 * nr2;
		System.out.println(korrutis);
		
		
		
	}

}
