package praktikum2;

public class Andmed {
	
	public static void main(String[] args){
		
		String nimi; //nimi on string
		int vanus; // vanus on number
		int kinganumber; //kindanumber on number
		String sugu; //sugu on string
		
		nimi = "Anneli"; //anname muutujale nimi väärtuse Anneli
		vanus = 20; //anname muutujale vanus väärtuse 20
		kinganumber = 37; //anname muutujale  kinganumber väärtuse 37
		sugu = "naine"; //anname muutujale sugu väärtuse naine
		
		System.out.println(nimi +"\n"+ vanus +"\n"+ kinganumber +"\n"+ sugu); //trükib välja nime, vanuse, kindanumbri ja soo erinevatel ridadeö
		
	} //end main()

} //end class Andmed
