package praktikum2;

import praktikum1.TextIO;

public class LetterReplace {
	

	public static void main(String[] args) {
		
	
		String text;
		
		System.out.println("Kirjuta midagi ning vaata, mis juhtub.");
		text = TextIO.getlnString();
				
		System.out.println("Teie sisestus oli " + text + " Kuid maagia tegi hoopis nii \n" + text.replace('a', '_'));
		
	}
		
		
}
