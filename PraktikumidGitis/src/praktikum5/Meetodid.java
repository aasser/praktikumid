package praktikum5;

import praktikum1.TextIO;

public class Meetodid {
	//meetodid ei tohi üksteise sisse panna, kuid neid saab esile kutsuda üksterisest
	public static void main(String[] args) {
		
		System.out.println(Meetodid.myMethod(3, "Tere 123")); //klass Meetodid, meetodi nimi myMethod() // meetod ei tagasta midagi
		int arv = TextIO.getlnInt(); //klass TextIO, meetodi nimi getlnInt()
		System.out.println(Math.abs(-9)); // klass Math, meetodi nimi abs() //meetod tagastab abs väärtuse
	}
	
	public static String myMethod(int mituKorda, String lisaTekst){ // sulgudesse saab argumendid
		String tagastus = "";
		
		for (int i = 0; i < mituKorda; i++) {
			tagastus += "See on text \n";
		}
		//System.out.println("Olen vahva!"); // see lihtsalt prindib ekraanile, ei tagasta midagi.
		
		//return "See text on pärit myMethod'i seest"; //see tagastab stringi
		return tagastus;
	}

}

