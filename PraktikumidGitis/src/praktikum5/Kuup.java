package praktikum5;

import praktikum1.TextIO;

public class Kuup {
	
	
	public static void main(String[] args) {
	System.out.println("Anna arv");
	int arv = TextIO.getlnInt();
	int arvKuubis = Kuup.kuup(arv); //võtab kasutaja sisestatud arvu
	System.out.println("Vastus:");
	System.out.println(arvKuubis);
	
	}
	
	public static int kuup(int inputValue){ //kasutaja sisestatud arvu kasutatakse inputValue'na
		
		int tagastus = (int) Math.pow(inputValue, 3);

		return tagastus;
		
	}
	

}
