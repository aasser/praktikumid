package praktikum5;

import praktikum1.TextIO;

public class KullKiri {
	public static void main(String[] args) {  
		
		int kasutajaSisestus = KullKiri.kasutajaSisestus("Kull või kiri", 0, 1);
		
		int randomNumber = KullKiri.randomNumber(0, 1);
		
		System.out.println(randomNumber);
		System.out.println(kasutajaSisestus);
		
		if(randomNumber == kasutajaSisestus){
			System.out.println("Panid Täppi");
		}else{
			System.out.println("Vale");
		}
	}
	
	public static int randomNumber(int min, int max){
		
		int vahemik = max - min ;
		
		//System.out.println(Math.random());
		//System.out.println(Math.random() * vahemik);
		//System.out.println((int)(Math.random() * (vahemik +1) ));
		return min + (int)(Math.random() * (vahemik +1));
		
	}
	
	
	
	public static int kasutajaSisestus(String kysimus, int min, int max){
		
		kysimus = ("Kull " + min + " või Kiri " + max);
		System.out.println(kysimus);
		int sisestus = TextIO.getInt();	
		return sisestus;
		
		
	}
	

}
