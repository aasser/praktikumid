package praktikum8;

public class Answer {

	public static void main(String[] args) {
		int[][] res = korraTabel(9);
		printMatrix(res);
	}

	public static int[][] korraTabel(int n) {
		int[][] res = new int[n][n];
		
		for(int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                res[i][j] = (int) (i*j);
            }
        }		
		return res; // TODO!!! Your code here
	}
	
	public static void printMatrix(int[][] res){
        for (int[] row : res){
            for (int col : row){
                System.out.printf("%2d ", col);
            }
            System.out.println();
        }

}
}