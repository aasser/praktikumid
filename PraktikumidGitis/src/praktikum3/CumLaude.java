package praktikum3;

import praktikum1.TextIO;

public class CumLaude {

	public static void main(String[] args) {

		double keskmine;
		double l6put88;

	
			System.out.println("Kasutaja, sisesta palun oma keskmine hinne.");
			keskmine = TextIO.getlnDouble();
			System.out.println("Keskmine hinne sobib");

			if (keskmine > 5 && keskmine < 0.000000000000001)
				System.out.println("Väär");


			System.out.println("Kasutaja, sisesta palun oma l6put88 hinne.");
			l6put88 = TextIO.getlnDouble();
			System.out.println("Lõputöö hinne sobib");
			
			if (l6put88 > 5 && l6put88 < 0.000000000000001)
				System.out.println("Väär");

		// if (keskmine > 5 && l6put88 > 5) {
		// System.out.println("Hinne ei saa olla suurem kui 5 ju o.o");
		// return;
		// }

		// if (keskmine < 0.000000000000001 && l6put88 < 0.000000000000001) {
		// System.out.println("Keskmine ei saa olla negatiivne ju o.o");
		// return;
		// }

		if (keskmine >= 4.5 && l6put88 == 5) {
			System.out.println("Jah saad cum laude diplomile!");
			return;
		} else {
			System.out.println("Ei saa CumLaudet ");
		}
		return;
	}
}
// Parem on siis kui kontrollida kohe kasutaja poolt sisestatud andmeid
