package praktikum3;

import praktikum1.TextIO;

public class Tehisintellekt {

	public static void main(String[] args) {

		int esimeneVanus;
		int teineVanus;

		System.out.println("Tahan kahte vanust");
		System.out.println("Kasutaja, sisesta palun esimene vanus");
		esimeneVanus = TextIO.getlnInt();
		System.out.println("Sisestasid: " + esimeneVanus);
		System.out.println("Kasutaja, sisesta palun teine vanus");
		teineVanus = TextIO.getlnInt();
		System.out.println("Sisestasid: " + teineVanus);

		int vahe = Math.abs(esimeneVanus - teineVanus);

		if (vahe >= 5 && vahe < 10) {
			System.out.println("Vanuste vahe on 5 aastat või rohkem. Aww");
		}

		if (vahe >= 10) {
			System.out.println("Vanuste vahe on juba 10 aastat või rohkem. Rawr");
		}

		if (vahe < 5) {
			System.out.println("Moew! Täitsa sobib ju, vahe alla 5 aasta");
		}

	}

}
