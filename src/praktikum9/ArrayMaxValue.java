package praktikum9;

import java.util.Arrays;

import praktikum8.ArrayGen;

public class ArrayMaxValue {
	
	public static void main(String[] args){
	int[] massiiv = {1, 3, 6, 7, 8, 3, 5, 7, 21, 3};
	
	System.out.println(Arrays.toString(massiiv));
	System.out.println("\nMaximum: " + maksimum(massiiv));
	
	int[][] neo = {
		    {1, 3, 6, 7},
		    {2, 3, 3, 1},
		    {17, 4, 5, 0},
		    {-20, 13, 16, 20}
		};
	ArrayGen.printMatrix(neo);
	System.out.println("\nMaximum: " + maxMatrix(neo));

	}
	
	public static int maksimum(int[] masiiv){
		int max = Integer.MIN_VALUE;
		for(int i = 0; i < masiiv.length; i++) {
		      if(masiiv[i] > max) {
		         max = masiiv[i];
		      }
		}
		
		return max;
		
	}
	
	public static int maxMatrix(int[][] neo){
		int maxValue = Integer.MIN_VALUE;
        for (int[] row : neo){
            for (int col : row){
                if(col > maxValue){
                    maxValue = col;
                }
            }
        }
		return maxValue;
	}
}
