package praktikum6;

import praktikum1.TextIO;

public class Betting {
	
	public static void main(String[] args) {
		
		//int algus = 100;
		
		
		//System.out.println("Said 100 euri.");
		//System.out.println("Panusta.");
		
		
		int raha = 100;
		
		while (true){
			
			int sisestus = kasutajaSisestus("", 1, 25);
			int suvaline = randomNumber(0,1);
			System.out.println("Maksimaalne panus on 25!");
			System.out.println("Panustasid: " + sisestus);			
			System.out.println("Kull 0 või kiri 1: " + suvaline);
			//raha = raha - sisestus;
			
			
			if(suvaline == 1){
				raha = raha + sisestus * 2;
				System.out.println("Tulemus: " + raha + " €");
			}			
			if(suvaline == 0){
				raha = raha - sisestus;
				System.out.println("Tulemus: " + raha + " €");
			}
			
			if(raha == 0){
				
				System.out.println("Raha sai otsa" + raha + "€");
				return;
			}
		}
		
		
		
	}
	
	public static int randomNumber(int min, int max){
		
		int vahemik = max - min ;
		return min + (int)(Math.random() * (vahemik +1));
		
	}
	
	public static int kasutajaSisestus(String kysimus, int min, int max) {

		kysimus = ("Panusta! " + min + " kuni " + 25 + ": ");
		System.out.print(kysimus);
		int sisestus = TextIO.getlnInt();
		return sisestus;

	}

}
