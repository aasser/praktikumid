package praktikum12;

public class Matrix {
	public static void main(String[] args) {
		int[][] neo = { 
				{ 1, 1, 1, 1, 1 }, 
				{ 2, 3, 4, 5, 6 }, 
				{ 3, 4, 5, 6, 7 }, 
				{ 4, 5, 6, 7, 8 },
				{ 5, 6, 7, 8, 9 }, };

		int[] neo2 = { 1, 1, 1, 1, 1 };

		System.out.println("Ühe realine masiiv ");
		Matrix.tryki(neo2);
		System.out.println("\n");
		System.out.println("Mitme realine masiivi maatriks");
		Matrix.tryki(neo);
		System.out.println("\nMitme realine masiivi maatriksi transponeer");
		Matrix.transponeeri(neo);
		System.out.println("\n");
		// maatriksi iga rea elementide summa
		System.out.println("Maatriksi iga rea elementide summa");
		tryki(ridadeSummad(neo));
		System.out.println("\n");
		System.out.println("Maatriksi peadiagonaali summa\n" + peaDiagonaaliSumma(neo));
		System.out.println("\nMaatriksi kõrvaldiagonaali summa\n" + korvalDiagonaaliSumma(neo));
		
		System.out.println("\nMaatriksi maksimumid igas reas\n");
		tryki(ridadeMaksimumid(neo));
		System.out.println("\nMaatriksi minimum tervest maatriksist\n" + miinimum(neo));
		
		
	}
	
	public static int miinimum(int[][] maatriks) {

		int minimum = Integer.MAX_VALUE;

		// int minimum = maatriks[0][0];

		for (int[] rida : maatriks) {
			for (int i : rida) {
				if (i < minimum) {
					minimum = i;
				}

			}

		}
		return minimum;
	}
	
	public static int[] ridadeMaksimumid(int[][] maatriks){

	    int[] maksimumid = new int[maatriks.length];
        for (int i = 0; i < maatriks.length; i++) {
        	maksimumid[i] = reaMaksimum(maatriks[i]);
			
		}
        
        //summad[i] = reaSumma(maatriks[i]);
		return maksimumid;
	}
	
	public static int reaMaksimum(int[] maatriks){
		
		int maksimum = maatriks[0];
		for (int i = 0; i < maatriks.length; i++) {
			
			if (maatriks[i] > maksimum) {
				maksimum = maatriks[i];
			}
		}
		
		return maksimum;
		
	}

	

	public static int korvalDiagonaaliSumma(int[][] maatriks) {
		int i;
		int sum = 0;
		for (i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks.length; j++) {
				if (i + j == maatriks.length - 1) {
					sum += maatriks[i][j];
				}
			}

		}
		// System.out.println(sum);
		return sum;

	}

	// Liidame kokku peadiagonaali elemendid
	public static int peaDiagonaaliSumma(int[][] neo) {

		int i;
		int sum = 0;

		// Liidame kokku peadiagonaali elemendid
		for (i = 0; i < neo.length; i = i + 1) {
			sum = sum + neo[i][i];
		}
		// System.out.println(sum);
		return sum;

	}

	// Trykime masiivi välja
	public static void tryki(int[] masiiv) {
		int i;
		for (i = 0; i < masiiv.length; i = i + 1) {
			System.out.print(masiiv[i] + " ");
		}
	}

	// Trykime maatriksi välja
	public static void tryki(int[][] maatriks) {
		int i, j;

		// Trykime maatriksi välja
		for (i = 0; i < maatriks.length; i = i + 1) {
			for (j = 0; j < maatriks[i].length; j = j + 1) {
				System.out.print(maatriks[i][j] + " ");
			}
			System.out.println();
		}

	}

	public static int[][] transponeeri(int[][] maatriks) {
		int i, j;
		// Transponeerimine
		int[][] morpheus = new int[maatriks[0].length][maatriks.length];

		for (i = 0; i < maatriks.length; i = i + 1) {
			for (j = 0; j < maatriks[i].length; j = j + 1) {
				morpheus[j][i] = maatriks[i][j];
			}
		}

		// Trykime transponeeritud maatriksi välja
		for (i = 0; i < morpheus.length; i = i + 1) {
			for (j = 0; j < morpheus[i].length; j = j + 1) {
				System.out.print(morpheus[i][j] + " ");
			}
			System.out.println();
		}
		return morpheus;

	}

	public static int reaSumma(int[] masiiv) {
		int sum = 0;

		for (int i : masiiv) {
			sum += i;

		}
		return sum;
	}

	public static int[] ridadeSummad(int[][] maatriks) {
		int i;
		int[] summad = new int[maatriks.length];

		for (i = 0; i < maatriks.length; i++) {
			summad[i] = reaSumma(maatriks[i]);

		}

		return summad;

	}

}
