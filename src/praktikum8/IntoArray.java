package praktikum8;

import java.util.ArrayList;

import praktikum1.TextIO;

public class IntoArray {
	public static void main(String[] args) {

		// String nimi = new String("Pille");
		// Inimene keegi = new Inimene("Juku", 50);
		// Inimene keegiveel = new Inimene("Peeter", 20);

		// keegi.tervita();

		ArrayList<Inimene> inimesed = new ArrayList<Inimene>();
		String name;
		int age;
		

		do {
			System.out.println("Sisesta inimese nimi");
			name = TextIO.getlnString();
			System.out.println("Sisesta inimese vanus");
			age = TextIO.getlnInt();
			if (age <= 0) {
				System.out.println("Sisestasid vale vanuse programm lõpetab töö.\nVale vanusega inimest nimega " + name + " ei kuvata");
				break;
			}

			inimesed.add(new Inimene(name, age));

		} while (age >= 1);

		for (Inimene inimene : inimesed) {
			// Java kutsub välja Inimene klassi toString() meetodi
			// System.out.println(inimene);
			inimene.tervita();
		}

		// Samasugune for tsykkel nagu eelmine
		// for (int i = 0; i < inimesed.size(); i++) {
		// inimene.tervita();
		// }

	}
}
