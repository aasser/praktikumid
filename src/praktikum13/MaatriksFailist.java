package praktikum13;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class MaatriksFailist {
	public static void main(String[] args) {

		// punkt tähistab jooksvat kataloogi
		String kataloogitee = FailiLugeja.class.getResource(".").getPath();

		// otsime samast kataloogist kala.txt-nimelist faili
		File file = new File(kataloogitee + "maatriks.txt");

		try {
			// avame faili lugemise jaoks
			BufferedReader in = new BufferedReader(new FileReader(file));
			String rida;
			List<Integer> lineList = new ArrayList<Integer>();
			// loeme failist rida haaval
			while ((rida = in.readLine()) != null) {
				double nr = Double.parseDouble(rida);
				lineList.add((int) nr);
			}
			
			//System.out.println(lineList);
			Integer[] masiiv = new Integer[lineList.size()];
			masiiv = lineList.toArray(masiiv);
			System.out.println(masiiv);
		} catch (FileNotFoundException e) {
			System.out.println("Faili ei leitud: \n" + e.getMessage());
		} catch (Exception e) {
			System.out.println("Error, jee, mingi muu error: " + e.getMessage());
		}
	}
}
