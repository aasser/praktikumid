package praktikum13;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
//kõikide asjade impordiks ctrl+shift+o

public class NumbridFailist {

	public static void main(String[] args) {
		
		String kataloogitee = FailiLugeja.class.getResource(".").getPath();
	    ArrayList<String> failiSisu = FailiLugeja.loeFail(kataloogitee + "numbrid.txt");
	    System.out.println(failiSisu);
	    Collections.sort(failiSisu);
	    System.out.println(failiSisu);

	    double sum = 0;
	    double avr = 0;
	    for(String rida: failiSisu){
	    	double nr;
	    	try {
	    		nr = Double.parseDouble(rida);
	    		sum += nr;
	    		avr = sum / failiSisu.size();
			} catch (NumberFormatException e) {
				nr = 0;// TODO: handle exception
			}
	    }
	 System.out.println(avr);

	}
}
