package praktikum14;

public class Ring {

	Punkt keskpunkt;
	double raadius;

	//public Ring(Punkt punn, Joon joon1) {
	//	keskpunkt = punn;
	//	raadius = joon1;
	//}

	public Ring(Punkt punn, double i) {
		keskpunkt = punn;
		raadius = i;

	}

	public double pindala() {

		double pind = (Math.pow(raadius, 2) * Math.PI);

		return pind;
	}

	public double ymberm66t() {
		double ymber = 2* Math.PI * raadius;
		return ymber;

	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Keskpunkt :" + keskpunkt + " Raadius :" + raadius + " Ümbermõõduga :"+ ymberm66t() + " Pindalaga "+ pindala();
	}

}
