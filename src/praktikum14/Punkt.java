package praktikum14;

public class Punkt {
	

	int x ;
	int y ;

	public Punkt(int x, int y) {
		
		//"this" kasutati sellepärast, et on kaks samasugust muutujat
		this.x = x;
		this.y = y;
		//Luuakse punkt objekt koordinate x,y kasutades
	}

	public Punkt() {
		//Luuakse punkt objekt koordinate x,y määramata
	}
	
	
	@Override //annotatsioon // annotation  - override aitab välistada vigu
	public String toString() {
		// TODO Auto-generated method stub
		return "("+ x + " , " + y + ")";
	}
	
	
	
}
