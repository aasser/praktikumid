package praktikum14;

public class Joon {

	Punkt punn1;
	Punkt punn2;

	public Joon(Punkt x, Punkt y) {
		punn1 = x;
		punn2 = y;
	}


	@Override
	public String toString() {
		return "Prindin joone " + punn1+ " " + punn2 + "Pikkusega" + pikkus();
	}
	
	public double pikkus() {

		double a = punn2.x - punn1.x;
		double b = punn2.y - punn1.y;

		double punn1Ruut = Math.pow(a, 2);
		double punn2Ruut = Math.pow(b, 2);

		double pikk = Math.sqrt(punn1Ruut + punn2Ruut);

		return pikk;

	}

	
	
}
