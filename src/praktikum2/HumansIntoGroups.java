package praktikum2;

import praktikum1.TextIO;

public class HumansIntoGroups {

	public static void main(String[] args) {
		
		double humans;
		double groups;
		int whole;
		double into;
		
		System.out.println("Mitu inimest on?");
		humans = TextIO.getlnDouble();
		System.out.println("Mitmestesse gruppidesse soov on inimesed jaotada?");
		groups = TextIO.getlnDouble();
		System.out.println("Teil on " + humans + " inimest. Te tahate nad jagada " + groups + " gruppi.");
		whole = (int) humans / (int) groups;
		into = humans % groups;
		System.out.println("Täis gruppe saab "+ whole + " ja inimesi jääb üle " + into);
		
		
	}
	
	
}
