package praktikum11;

import java.applet.Applet;
import java.awt.*;
import java.awt.geom.Rectangle2D;

import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

public class PudeliHari extends Applet {
	/*
	 * Ringjoone vo~rrand parameetrilisel kujul x = r * cos(t) y = r * sin(t) t
	 * = -PI..PI
	 */
	public void paint(Graphics g) {
		// int x0 = 150; // Keskpunkt
		// int y0 = 150;
		// int r = 100; // Raadius
		// int x, y;
		double t;

		// Kysime kui suur aken on?
		Dimension d = getSize();
		int w = d.width / 2;
		int h = d.height / 2;
		int x0 = d.width / 2;
		int y0 = d.height / 2;
		int x = d.width;
		int y = d.height;
		int r = (int) ((d.width < d.height) ? 0.4 * d.width : 0.4 * d.height);
		// int w = getWidth();
		// int h = getHeight();

		// Ta"idame tausta
		//g.setColor(Color.BLACK);
		//g.fillRect(0, 0, x, y);

				
		//Taust on gradient white to black
		Graphics2D g2 = (Graphics2D) g;
		GradientPaint blackToGray = new GradientPaint(0, 0, Color.WHITE, 0, y, Color.BLACK);
		g2.setPaint(blackToGray);
		g2.fillRect(0, 0, x, y);
		//g2.fill(new Rectangle2D.Double(0, 0, x, y));

		
		// Joonistame
		g.setColor(Color.RED);
		for (t = -Math.PI; t < Math.PI; t = t + Math.PI / 16) {
			x = (int) (r * Math.cos(t) + x0);
			y = (int) (r * Math.sin(t) + y0);
			g.drawLine(x0, y0, x, y);
		}
	}

	

}