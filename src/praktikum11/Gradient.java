package praktikum11;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

public class Gradient extends Applet{
	
	public void paint(Graphics g) {
		
		Dimension d = getSize();
		int x = d.width;
		int y = d.height;
		
		//Graphics2D g2 = (Graphics2D) g;
		//GradientPaint blackToGray = new GradientPaint(100, 50, Color.WHITE, 100, 400, Color.BLACK);
		//g2.setPaint(blackToGray);
		//g2.fill(new Rectangle2D.Double(0, 0, x, y));
		double varvimuutus = 255. / y;
		
		
		for (int i = 0; i < y; i++) {
			int varvikood = (int) (255-i * varvimuutus);
			Color minuvarv = new Color(varvikood,varvikood,varvikood);
			g.setColor(minuvarv);
			//g.drawLine(x1, y1, x2, y2)
			g.drawLine(0, i, x, i);
			
		}
		
	}

}
