package praktikum10;

import java.io.File;
import java.util.Arrays;

public class FailiNimekiri {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// asukoht /
		// trykiFailid("/");
		// asukoht /home/aasser/Documents/git/Praktikumid
		trykiFailid("/home/aasser/Documents/git/Praktikumid");
	}

	public static void trykiFailid(String kataloogiTee) {

		File kataloog = new File(kataloogiTee);
		File[] failid = kataloog.listFiles();
		Arrays.sort(failid);

		// for each tükkel tegelikult
		for (File file : failid) {
			if (file.isDirectory()) {
				trykiFailid(file.getAbsolutePath());
				System.out.println("Kataloog1: " + file.getName());

			} else {
				System.out.print("Fail:     ");
			}
			System.out.println(file.getName());
		}

	}

}
