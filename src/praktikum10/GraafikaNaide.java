package praktikum10;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class GraafikaNaide extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("JavaFX-iga joonistatud kloun");
		Group root = new Group();
		Canvas canvas = new Canvas(400, 400);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		joonista(gc);
		root.getChildren().add(canvas);
		primaryStage.setScene(new Scene(root));
		primaryStage.show();
	}

	private void joonista(GraphicsContext gc) {
		// gc.setFill(Color.GREEN);
		// nii saab teha ise värve
		// Color minuV2rv = Color.rgb(red, green, blue);
		// gc.setLineWidth(5);
		// gc.fillRoundRect(x, y, w, h, arcWidth, arcHeight);
		// gc.fillRoundRect(50, 50, 300, 300, 40, 40);
		// gc.strokeLine(x1, y1, x2, y2);
		// gc.strokeLine(100, 300, 300, 300);
		// gc.strokeOval(x, y, w, h);

		// kael
		gc.setFill(Color.WHEAT);
		gc.fillRoundRect(175, 240, 50, 40, 20, 20);

		// Keha
		gc.setFill(Color.PURPLE);
		gc.fillRoundRect(75, 260, 250, 250, 20, 20);

		// lips vasak pool
		gc.setFill(Color.RED);
		double xpoints[] = { 242, 242, 200 };
		double ypoints[] = { 245, 290, 270 };
		int num = 3;
		gc.fillPolygon(xpoints, ypoints, num);
		// lips parem pool
		gc.setFill(Color.RED);
		double xpoints1[] = { 160, 160, 200 };
		double ypoints1[] = { 245, 290, 270 };
		int num1 = 3;
		gc.fillPolygon(xpoints1, ypoints1, num1);
		// lipsu kesk
		gc.setFill(Color.RED);
		gc.fillOval(190, 255, 20, 30);

		// näo põhi
		gc.setFill(Color.WHEAT);
		gc.fillOval(100, 50, 200, 200);

		// juuksed vasak pool
		gc.setFill(Color.ORANGE);
		gc.fillOval(100, 55, 50, 50);
		gc.fillOval(90, 80, 30, 30);
		gc.fillOval(115, 85, 35, 30);
		gc.setStroke(Color.WHITE);
		gc.strokeOval(100, 55, 50, 50);

		// Juuksed parem pool
		gc.setFill(Color.ORANGE);
		gc.fillOval(250, 55, 50, 50);
		gc.fillOval(275, 80, 30, 30);
		gc.fillOval(250, 85, 35, 30);
		gc.setStroke(Color.WHITE);
		gc.strokeOval(250, 55, 50, 50);

		// Müts kolmnurk
		gc.setFill(Color.BLACK);
		// double xpoints[] = { 100, 200, 300 };
		// double ypoints[] = { 80, 0, 80};
		// int num = 3;
		// gc.fillPolygon(xpoints, ypoints, num);

		// Müts tophat
		gc.fillRoundRect(100, 50, 200, 35, 60, 60);
		gc.fillRoundRect(120, 0, 160, 70, 60, 60);
		gc.setFill(Color.WHITE);
		gc.fillRoundRect(120, 49, 160, 2, 1, 1);

		// Blush
		gc.setFill(Color.RED);
		gc.fillOval(110, 139, 35, 35);
		gc.fillOval(255, 139, 35, 35);

		// silma sisemused valged
		gc.setFill(Color.WHITE);
		gc.fillOval(150, 90, 50, 70);
		gc.fillOval(200, 90, 50, 70);

		// Silma puhkpillid
		gc.setFill(Color.BLACK);
		gc.fillOval(170, 120, 10, 10);
		gc.fillOval(220, 120, 10, 10);

		// silma kontuur sinine
		gc.setStroke(Color.BLUE);
		// silma puhkpilli stroke
		gc.strokeOval(158, 121, 35, 10);
		gc.strokeOval(208, 121, 35, 10);

		// silma make-up ääre stroke
		// gc.strokeOval(150, 100, 50, 50);
		// gc.strokeOval(200, 100, 50, 50);

		// See on suu valge
		gc.setStroke(Color.WHITE);
		gc.setLineWidth(30);
		gc.strokeRoundRect(155, 183, 90, 40, 60, 60);
		// See on nina
		gc.setFill(Color.RED);
		gc.fillOval(182, 139, 35, 35);

		// Huuled
		gc.setFill(Color.RED);
		gc.fillRoundRect(155, 183, 90, 40, 100, 100);

		// suu sisemus
		gc.setFill(Color.DARKRED);
		gc.fillRoundRect(170, 190, 60, 25, 100, 100);
		// hambad
		gc.setFill(Color.WHITE);
		// ülemised
		gc.fillRoundRect(185, 190, 30, 5, 50, 50);
		// alumised
		gc.fillRoundRect(185, 210, 30, 5, 50, 50);

		gc.setFill(Color.WHITE);
		gc.fillText("Missed me, kids?MWAHAHAHAHAHAH", 100, 370);
	}
}