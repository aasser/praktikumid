package praktikum10;

public class Fibonacci {

	public static void main(String[] args) {
		
		// while (i < 20) {
		// System.out.println("tere " + i);
		// i++;
		// }
		//
		// for (int j = 0; j < 10; j++) {
		// System.out.println("tere for " + j);
		// }
		
		//suurendab n väärtust lõpmatult ühe võrra
		int i = 0;
		while (true) {
			System.out.println(i + " " + fibonacci(i));
			i++;
		}

	}

	//et fibonacci tagastaks mitte negatiivseid arve vahepeal, siis kasutada long ; int asemel
	public static int fibonacci(int n) {
		// xn = xn-1 + xn-2
		// x =fibonacci
		//fibonacci(n-1) + fibonacci(n-2)

		int f0 = 0;
		int f1 = 1;

		//kui n on null, siis tagastab 0
		if (n == f0) {
			return 1;
		}
		//kui n on üks, siis tagastab 1
		if (n == f1) {
			return 0;
		}
		return fibonacci(n - 1) + fibonacci(n - 2);

	}
}
