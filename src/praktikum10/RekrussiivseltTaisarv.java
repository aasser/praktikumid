package praktikum10;

public class RekrussiivseltTaisarv {

	public static void main(String[] args) {
		System.out.println(astenda(2, 4));

	}

	public static int astenda(int arv, int aste) {

		// lõpetamise tingimus
		if (aste >= 1) {
			return arv * astenda(arv, aste - 1);

		} else
			return 1;

	}

}
